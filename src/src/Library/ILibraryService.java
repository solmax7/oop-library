package src.Library;

public interface ILibraryService {

    public  boolean addBook(Library library, Book book);

    public  boolean deleteBook(Library library, String nameBook);

    public  boolean SearchBook(Library library, String nameBook);

    //boolean addBook(Library library, Book book);

    }
