package src.Library;
//import Library.LibraryServiceImpl

public class DemoService {

    private ILibraryService LibraryService;
    private Library library;

    public DemoService(ILibraryService libraryService, Library library) {
        this.LibraryService = libraryService;
        this.library = library;
    }


    public void addBookDemo(){

       // Library lib1 = new Library();

        LibraryService.addBook( library ,new Book("Idiot", "Dostoevskiy"));
        LibraryService.addBook( library ,new Book("Tihiy Don", "M. Sholohov"));
        LibraryService.addBook( library ,new Book("Vsadnik bez golovi", "M. Rid"));
        LibraryService.addBook( library ,new Book("Евгений Онегин", "Пушкин"));
        LibraryService.addBook( library ,new Book("Том Сойер", "Марк Твен"));
        LibraryService.addBook( library ,new Book("Хорстман", "Ява"));
        LibraryService.addBook( library ,new Book("Техника моложежи", "Журналы"));
        LibraryService.addBook( library ,new Book("Война и мир", "Толстой"));
        LibraryService.addBook( library ,new Book("Отцы и дети", "Тургенев"));
        LibraryService.addBook( library ,new Book("Черный обелиск", "Ремарк"));

         showBook(library);

    }

    public void addBookDemoOne(){

        LibraryService.addBook( library ,new Book("12 стульев", "Ильф и Петров"));

    }

    public void SearchBookDemo(){

        // Library lib1 = new Library();

        boolean fd =  LibraryService.SearchBook(library, "Отцы и дети");

        if (fd)
            System.out.println("Книга найдена");
        else
            System.out.println("Книги в библиотеке нет");

        //showBook(library);

    }

    public void execute() {
        System.out.println("Добавление книг в библиотеку!!!");
        System.out.println("----");
        addBookDemo();
        System.out.println("Удаление книги!!!");
        System.out.println("----");
        DeleteBookDemo();

        System.out.println("Поиск книги!!!");
        System.out.println("----");
        SearchBookDemo();
        System.out.println("----");
        System.out.println("Добавление еще одной книги!!!");
        System.out.println("----");
        addBookDemoOne();
        // showBook(library);
       // showBook(library);

     //   checkDelete();
      //  checkSearch();
    }

    public void DeleteBookDemo(){

       // Library lib1 = new Library();

        LibraryService.deleteBook( library ,"Idiot");

        showBook(library);

    }

    public  void showBook(Library lib){

        Book showbook[] = lib.getLibrary();

        for (int i = 0; ((Book[]) showbook).length > i; i++) {

            if (showbook[i] !=null)
                System.out.println(showbook[i]);

        }

        System.out.println();
    }

    public  void searchBook (Library lib, String name){


        boolean flagAdd = false;


        for (int i = 0; i < lib.getLibrary().length; i++) {

            String d = "";
            if (lib.getLibrary()[i].getName() != null && lib.getLibrary()[i].getName().equals(name) && flagAdd == false){

                System.out.println("Результаты поиска: " + lib.getLibrary()[i]);
                flagAdd = true;
                break;
            }

        }




    }

//    public static void main(String[] args) {
//
//
//        addTryBook();
//    }
//
//
//
//        LibraryServiceImpl.showBook(lib);
//
//        lib.deleteBook(lib, "Idiot");
//
//        LibraryServiceImpl.showBook(lib);
//
//        LibraryServiceImpl.searchBook(lib, "Tihiy Don");
//
//        Book book10 = new Book();
//        book10.setName("Доктор Живаго");
//        book10.setWriter("Пастернак");
//
//        LibraryServiceImpl.addBook(lib, book10);
//
////



}
