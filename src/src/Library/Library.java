package src.Library;

public class Library {

    private Book[] library;
    private String nameLibrary;

    public Library() {

        library = new Book[10];
    }

    public Book[] getLibrary() {

        return library;

    }

    public int LibraryLenght(){

        return library.length;

    }

    public void setLibrary(Book[] library) {
        this.library = library;
    }

    public String getNameLibrary() {
        return nameLibrary;
    }

    public void setNameLibrary(String nameLibrary) {
        this.nameLibrary = nameLibrary;
    }

}
